( function () {
    var app = angular.module ( "DiceGame", [ 'ngResource' ] ),
        gameSize = 20,
        DiceGame = require ( "./game.js"),
        game = new DiceGame ( gameSize );

    game.addSpecial( 12, game.end )
        .addSpecial ( 19, game.shiftTo.bind( game, 11 ) );

    app.factory ( "DiceGame", function () {
        return game;
    } );

    app.factory( "Statistics", function( $resource ) {
        return $resource( "/v1/user/:userId/statistics/", {
            userId: 123
        } );
    });

    require ( "./GameController" ) ( app );

} () );