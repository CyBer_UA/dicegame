module.exports = function ( app  ) {

    var prevActive = 0;

    app.controller ( "GameController", [ "$scope", "Statistics", "DiceGame",
        function ( $scope, Statistics, game ) {
            var arr = new Array ( game.getSize() );
            $scope.arr = arr;
            setActive();


            $scope.nextStep = function () {
                $scope.stats = game.shiftOn( Math.random() * 6 );
            };

            $scope.resetGame = function () {
                game.reset();
            };

            $scope.saveStatistics = function () {

                game.save( function ( data ) {
                    Statistics.save( data );
                } );

            };

            game.on( "position:shift", setActive );
            game.on( "game:end", function () {
                setTimeout( function () { // it will be showed after last field is selected
                    alert( "your win" );
                }, 0 );
            } );

            function setActive () {
                var active = game.getActive() - 1,
                    phase = $scope.$root.$$phase;

                active = active < 0 ? 0: active;
                arr [ prevActive ] = 0;
                arr [ ( prevActive = active ) ] = 1;

                if (phase != '$apply' && phase != '$digest') {
                    $scope.$apply();
                }
            }
        } ] );
};