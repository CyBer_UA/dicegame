function EventEmitter() {
    this._handlers = [];
    this._oncehandlers = [];
};


EventEmitter.prototype = {
    on: function( event ) {
        event = event.toLowerCase();
        var handlers = this._handlers[ event ] || [];

        this._handlers[ event ] = handlers.concat( handlers.slice.call( arguments, 1 ));

        return this;
    },

    once: function ( event ) {

        event = event.toLowerCase();
        var handlers = this._oncehandlers [ event ] || [];

        this._oncehandlers[ event ] = handlers.concat( handlers.slice.call( arguments, 1 ));

        return this;
    },

    off: function( event, handler ) {
        event =  event.toLowerCase();

        var handlers =  this._handlers[ event ],
            oncehandlers = this._oncehandlers [ event ];

        if( !handlers && !oncehandlers ) return;

        if( !handler ) {
            delete this._handlers[ event ];
            delete this._oncehandlers [ event ];
        }

        function replace( handlers ) {

            for( var i = 0; i < handlers.length; i++) {
                if ( handlers[ i ] === handler ) {
                    handlers.splice( i, 1 );
                    break;
                }
            }
        }

        handlers && replace( handlers );
        oncehandlers && replace( oncehandlers );

        return this;
    },

    emit: function( event ) {

        event = event.toLowerCase();

        var handlers = this._handlers[ event ],
            oncehandlers = this._oncehandlers [ event ],
            self = this,
            args = [].slice.call( arguments, 1 );

        setTimeout( function () {
            if( handlers  )  {
                callFnc( 0, handlers );
            }

            if ( oncehandlers ) {
                callFnc( 0, oncehandlers );
                delete self._oncehandlers [ event ] ;
            }

        } , 0 );

        function callFnc ( i, handlers ) {
            try {

                for(; i < handlers.length; i++) {
                    handlers [ i ].apply( self || window, args );
                }

            } catch ( err ) {

                self.emit( "error", err, handlers[ i ], event );

                if( ++i < handlers.length)
                    callFnc( i, handlers )
            }
        };

        return this;
    },

    getHandlers: function( event ) {
        var handlers = this._handlers[ event.toLowerCase() ];
        return handlers  || [];
    }
};


module.exports = EventEmitter;
