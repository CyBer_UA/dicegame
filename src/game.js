
var EventEmitter = require( "./EventEmitter.js" );

function DiceGame ( size ) {

    if ( typeof size != "number" ) {
        return;
    }

    this._size = size;
    EventEmitter.call ( this );
    this.reset();
};

DiceGame.prototype = Object.create( EventEmitter.prototype );

DiceGame.prototype.addSpecial = function ( number, callback ) {
    this.on ( "special:" + number, callback );
    return this;
};

DiceGame.prototype.removeSpecial = function ( number, callback ) {
    this.off ( "special:" + number, callback );
    return this;
};

DiceGame.prototype.getSize = function () {
    return this._size;
}

DiceGame.prototype.shiftOn = function ( number ) {

    if ( this._isEnded )
        return this.getStats();

    var diff;

    number = getNumber ( number, 1, 6 );
    this._updateStat ( number );

    this._currentPosition += number;
    diff = this._size - this._currentPosition;



    if ( diff < 0 ) {
        this._currentPosition = this._size + diff;
    }

    if ( diff == 0 ) {
        this.end();
    }

    this.emit( "position:shift" );
    this.emit( "special:" + this._currentPosition );

    return this.getStats();
};

DiceGame.prototype.shiftTo = function ( number ) {

    if ( isEmpty ( number ) || this._isEnded ) {
        return this;
    }

    this._currentPosition = getNumber ( number, 0, this._size );

    this.emit( "position:shift" );
    this.emit( "special:" + this._currentPosition );

    return this;
};


DiceGame.prototype.getStats = function ( ) {
    return this._stats;
};

DiceGame.prototype.getActive = function () {
    return this._currentPosition;
};

DiceGame.prototype._updateStat = function ( number ) {

    var stats = this._stats;

    stats.sum += number;
    stats.count ++;
    stats.avg = stats.sum / stats.count;
    stats.avg = stats.avg.toFixed(3);

};

DiceGame.prototype.end = function  ( ) {

    this._currentPosition = this._size;
    this._isEnded = true;
    this.emit( "game:end", this._stats );
    this.emit( "position:shift" );
    return this;
};

DiceGame.prototype.reset = function  ( ) {

    this._currentPosition = 0;
    this._stats = {
        count: 0,
        sum: 0
    };
    this._isEnded = false;
    this.emit ( "game:reset" );
    this.emit( "position:shift" );
    return this;
};

DiceGame.prototype.save = function  ( rest ) {
    rest ( this._stats );
};

function getNumber ( num, min, max ) {

    if ( isEmpty ( num ) ) {
        return 0;
    }

    num = Math.round( num );
    num = Math.max( min, num );
    num = Math.min ( max, num );

    return num;
}


function isEmpty ( val ) {

    return val === undefined ||
        val === null ||
        val !== val;
}

module.exports = DiceGame;

