/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	( function () {
	    var app = angular.module ( "DiceGame", [ 'ngResource' ] ),
	        gameSize = 20,
	        DiceGame = __webpack_require__ ( 2),
	        game = new DiceGame ( gameSize );

	    game.addSpecial( 12, game.end )
	        .addSpecial ( 19, game.shiftTo.bind( game, 11 ) );

	    app.factory ( "DiceGame", function () {
	        return game;
	    } );

	    app.factory( "Statistics", function( $resource ) {
	        return $resource( "/v1/user/:userId/statistics/", {
	            userId: 123
	        } );
	    });

	    __webpack_require__ ( 4 ) ( app );

	} () );

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	
	var EventEmitter = __webpack_require__( 3 );

	function DiceGame ( size ) {

	    if ( typeof size != "number" ) {
	        return;
	    }

	    this._size = size;
	    EventEmitter.call ( this );
	    this.reset();
	};

	DiceGame.prototype = Object.create( EventEmitter.prototype );

	DiceGame.prototype.addSpecial = function ( number, callback ) {
	    this.on ( "special:" + number, callback );
	    return this;
	};

	DiceGame.prototype.removeSpecial = function ( number, callback ) {
	    this.off ( "special:" + number, callback );
	    return this;
	};

	DiceGame.prototype.getSize = function () {
	    return this._size;
	}

	DiceGame.prototype.shiftOn = function ( number ) {

	    if ( this._isEnded )
	        return this.getStats();

	    var diff;

	    number = getNumber ( number, 1, 6 );
	    this._updateStat ( number );

	    this._currentPosition += number;
	    diff = this._size - this._currentPosition;



	    if ( diff < 0 ) {
	        this._currentPosition = this._size + diff;
	    }

	    if ( diff == 0 ) {
	        this.end();
	    }

	    this.emit( "position:shift" );
	    this.emit( "special:" + this._currentPosition );

	    return this.getStats();
	};

	DiceGame.prototype.shiftTo = function ( number ) {

	    if ( isEmpty ( number ) || this._isEnded ) {
	        return this;
	    }

	    this._currentPosition = getNumber ( number, 0, this._size );

	    this.emit( "position:shift" );
	    this.emit( "special:" + this._currentPosition );

	    return this;
	};


	DiceGame.prototype.getStats = function ( ) {
	    return this._stats;
	};

	DiceGame.prototype.getActive = function () {
	    return this._currentPosition;
	};

	DiceGame.prototype._updateStat = function ( number ) {

	    var stats = this._stats;

	    stats.sum += number;
	    stats.count ++;
	    stats.avg = stats.sum / stats.count;
	    stats.avg = stats.avg.toFixed(3);

	};

	DiceGame.prototype.end = function  ( ) {

	    this._currentPosition = this._size;
	    this._isEnded = true;
	    this.emit( "game:end", this._stats );
	    this.emit( "position:shift" );
	    return this;
	};

	DiceGame.prototype.reset = function  ( ) {

	    this._currentPosition = 0;
	    this._stats = {
	        count: 0,
	        sum: 0
	    };
	    this._isEnded = false;
	    this.emit ( "game:reset" );
	    this.emit( "position:shift" );
	    return this;
	};

	DiceGame.prototype.save = function  ( rest ) {
	    rest ( this._stats );
	};

	function getNumber ( num, min, max ) {

	    if ( isEmpty ( num ) ) {
	        return 0;
	    }

	    num = Math.round( num );
	    num = Math.max( min, num );
	    num = Math.min ( max, num );

	    return num;
	}


	function isEmpty ( val ) {

	    return val === undefined ||
	        val === null ||
	        val !== val;
	}

	module.exports = DiceGame;



/***/ },
/* 3 */
/***/ function(module, exports) {

	function EventEmitter() {
	    this._handlers = [];
	    this._oncehandlers = [];
	};


	EventEmitter.prototype = {
	    on: function( event ) {
	        event = event.toLowerCase();
	        var handlers = this._handlers[ event ] || [];

	        this._handlers[ event ] = handlers.concat( handlers.slice.call( arguments, 1 ));

	        return this;
	    },

	    once: function ( event ) {

	        event = event.toLowerCase();
	        var handlers = this._oncehandlers [ event ] || [];

	        this._oncehandlers[ event ] = handlers.concat( handlers.slice.call( arguments, 1 ));

	        return this;
	    },

	    off: function( event, handler ) {
	        event =  event.toLowerCase();

	        var handlers =  this._handlers[ event ],
	            oncehandlers = this._oncehandlers [ event ];

	        if( !handlers && !oncehandlers ) return;

	        if( !handler ) {
	            delete this._handlers[ event ];
	            delete this._oncehandlers [ event ];
	        }

	        function replace( handlers ) {

	            for( var i = 0; i < handlers.length; i++) {
	                if ( handlers[ i ] === handler ) {
	                    handlers.splice( i, 1 );
	                    break;
	                }
	            }
	        }

	        handlers && replace( handlers );
	        oncehandlers && replace( oncehandlers );

	        return this;
	    },

	    emit: function( event ) {

	        event = event.toLowerCase();

	        var handlers = this._handlers[ event ],
	            oncehandlers = this._oncehandlers [ event ],
	            self = this,
	            args = [].slice.call( arguments, 1 );

	        setTimeout( function () {
	            if( handlers  )  {
	                callFnc( 0, handlers );
	            }

	            if ( oncehandlers ) {
	                callFnc( 0, oncehandlers );
	                delete self._oncehandlers [ event ] ;
	            }

	        } , 0 );

	        function callFnc ( i, handlers ) {
	            try {

	                for(; i < handlers.length; i++) {
	                    handlers [ i ].apply( self || window, args );
	                }

	            } catch ( err ) {

	                self.emit( "error", err, handlers[ i ], event );

	                if( ++i < handlers.length)
	                    callFnc( i, handlers )
	            }
	        };

	        return this;
	    },

	    getHandlers: function( event ) {
	        var handlers = this._handlers[ event.toLowerCase() ];
	        return handlers  || [];
	    }
	};


	module.exports = EventEmitter;


/***/ },
/* 4 */
/***/ function(module, exports) {

	module.exports = function ( app  ) {

	    var prevActive = 0;

	    app.controller ( "GameController", [ "$scope", "Statistics", "DiceGame",
	        function ( $scope, Statistics, game ) {
	            var arr = new Array ( game.getSize() );
	            $scope.arr = arr;
	            setActive();


	            $scope.nextStep = function () {
	                $scope.stats = game.shiftOn( Math.random() * 6 );
	            };

	            $scope.resetGame = function () {
	                game.reset();
	            };

	            $scope.saveStatistics = function () {

	                game.save( function ( data ) {
	                    Statistics.save( data );
	                } );

	            };

	            game.on( "position:shift", setActive );
	            game.on( "game:end", function () {
	                setTimeout( function () { // it will be showed after last field is selected
	                    alert( "your win" );
	                }, 0 );
	            } );

	            function setActive () {
	                var active = game.getActive() - 1,
	                    phase = $scope.$root.$$phase;

	                active = active < 0 ? 0: active;
	                arr [ prevActive ] = 0;
	                arr [ ( prevActive = active ) ] = 1;

	                if (phase != '$apply' && phase != '$digest') {
	                    $scope.$apply();
	                }
	            }
	        } ] );
	};

/***/ }
/******/ ]);