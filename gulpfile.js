var gulp = require('gulp'),
    webpack = require ( "gulp-webpack" );

gulp.task( 'build', function(  ) {
   return gulp.src( "src/*.js" )
       .pipe(webpack( {
           entry: ["./src/app.js"],
           output: {
               filename: "app.js"
           }
       }))
       .pipe( gulp.dest( "dist/" ) );
});


gulp.task( 'watch', function() {
    gulp.watch( "src/*.js", [ "build" ] );
});

gulp.task ( "default", [ "build", "watch" ] );